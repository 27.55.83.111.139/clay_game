def check_word_length(word1: str, word2: str) -> bool:
    return (len(word1) == len(word2))

n = 4

def loading_data():
    words = [word.strip() for word in open("sowpods.txt").readlines()]

def check_word_length(word1: str, word2: str) -> bool:
    return (len(word1) == len(word2))

def three_letters_common(word: str, choice: str) -> str:
    return len(set(word) & set(choice)) == n - 1

def ordered(word: str, choice: str) -> bool:
    same = 0
    for pos, ch in enumerate(word):
        while word[pos] == choice[pos]:
            same += 1
    if same == n - 1:
        return True

def update(word: str, choices: list, reach: str) -> str:
    choice = word
    while choice != reach:
        choice = [choice for choice in choices if common(word, choice) and ordered(word, choice) and check_similarity(word, reach)] 
    return reach

def check_similarity(word: str, reach: str) -> bool:
    return any([True for w in word if w in reach])

choices = ['clad', 'play', 'gold', 'clay', 'goad', 'glad']
current = 'clay'
reach = 'gold'


print(update(current, choices, reach))

